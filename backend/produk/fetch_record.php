<?php
include'../../koneksi.php';
if($_POST['id']) {
    $id = $_POST['id']; //escape string
    $query = "SELECT * FROM produk WHERE id_produk=$id";
    //print_r($query);
    $result = mysqli_query($conn, $query);
    $data_p = mysqli_fetch_array($result);
    extract($data_p);
    $data='
          <form role="form" action="produk/pro_edit_produk.php" method="post" enctype="multipart/form-data">
          <input type="hidden" name="id" value="'.$id_produk.'">
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Kode Produk</span></label>
              <input type="text" class="form-control" name="kd_produk" placeholder="Kode Produk" required value="'.$kd_produk.'">
            </div>  
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Nama Produk</span></label>
              <input type="text" class="form-control" name="nm_produk" placeholder="Nama Produk" required value="'.$nm_produk.'">
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-home"></i> Deskripsi</span></label>
              <textarea class="form-control" name="desk_produk" required placeholder="Deskripsi">'.$desk_produk.'</textarea>
            </div>      
            <div class="form-group">
              <input type="hidden" name="gambar_lama" value="'.$image.'">
              <label><span><i class="fa fa-user"></i>Pilih Gambar</span></label>
              <input type="file" name="image" placeholder="pilih gambar">
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Sablon</span></label>
              <select name="sablon" class="form-control" value="'.$id_sablon.'">';
                $q_sablon = "SELECT id_sablon, nm_sablon FROM sablon";
                $res = mysqli_query($conn,$q_sablon);
                while($ks = mysqli_fetch_array($res)){ 
                $kd_s = $ks['id_sablon'];
                $nm_s = $ks['nm_sablon']; 
                $data.='<option value="'.$kd_s.'"';
                $data.=$s=($id_sablon==$kd_s) ? "selected" : '';
                $data.='>'.$nm_s.'</option>';}
                $data.='</select>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Bahan</span></label>
              <select name="bahan" class="form-control" value="'.$id_bahan.'">';
                $q_bahan = "SELECT id_bahan, nm_bahan FROM bahan";
                $res = mysqli_query($conn,$q_bahan);
                while($kb = mysqli_fetch_array($res)){ 
                $kd_b = $kb['id_bahan'];
                $nm_b = $kb['nm_bahan']; 
                $data.='<option value="'.$kd_b.'"';
                $data.=$s=($id_bahan==$kd_b) ? "selected" : '';
                $data.='>'.$nm_b.'</option>';}
                $data.='</select>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-circle "></i> Harga Jual</span></label>
              <input type="text" class="form-control" name="hrg_jual"  placeholder="Harga Jual"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')" required value="'.$harga_jual.'">
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Harga Beli</span></label>
              <input type="text" class="form-control" name="hrg_beli"  placeholder="Harga Beli"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')" required value="'.$harga_beli.'">
            </div>                       
            <div class="form-group">
              <label><span><i class="fa fa-globe"></i> Kategori</span></label>
              <select name="kd_kategori" class="form-control" value="'.$kd_kategori.'">';
                $q_kategori = "SELECT kd_kategori, nm_kategori FROM kategori";
                $res = mysqli_query($conn,$q_kategori);
                while($kat = mysqli_fetch_array($res)){ 
                $kd_k = $kat['kd_kategori'];
                $nm_k = $kat['nm_kategori']; 
                $data.='<option value="'.$kd_k.'"';
                $data.=$k=($kd_kategori==$kd_k) ? "selected" : '';
                $data.='>'.$nm_k.'</option>';}
                $data.='</select>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Tanggal Masuk</span></label>
              <input type="text" class="form-control" name="tgl_msk" placeholder="Tanggal Masuk" required value="'.$tgl_masuk.'">
            </div>  
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Stok</span></label>
              <input type="text" class="form-control" name="stok"  placeholder="Jumlah Stok"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')" required value="'.$balance.'">
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Berat</span></label>
              <input type="text" class="form-control" name="berat"  placeholder="Berat Produk"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'."''".')" required value="'.$berat.'">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-warning btn-block" name="submit"><span class="glyphicon glyphicon-off"></span> Update</button>
            </div>  
          </form>
    ';
echo $data;
    // Fetch Records
    // Echo the data you want to show in modal
 }
?>