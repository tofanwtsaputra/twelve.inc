<?php 
if (empty($_SESSION['user']==1)) 
{
  header('location:../../index.php');
}
 ?>
<ol class="breadcrumb">
  <li>
    <i class="fa fa-dashboard"></i>  <a href="index.php?p=dashboard">Dashboard</a>
  </li>                            
  <li class="active">
    <i class="fa fa-fw fa-folder-open"></i> Master
  </li>
  <li class="active">
    <i class="fa fa-image"></i> Baner
  </li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading"><strong><i class="fa fa-table"></i>  Tabel Baner</strong></div>
  <div class="panel-body">
    
  <button type="button" data-toggle="modal" data-target="#Add" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Baru</button>
  <br><br>         
  <table class="table table-responsive table-hover table-striped table-bordered example">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nama Baner</th>
        <th>Image</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <?php 
        include'../koneksi.php';
        $query = "SELECT *
                    FROM banner";
        $result = mysqli_query($conn, $query);
        
        $i = 0;
     ?>
    <tbody>
    <?php 
      while ($data = mysqli_fetch_array($result)) 
      {
          $i++;
     ?>
      <tr>
        <td><?php echo $data['id_banner']; ?></td>
        <td><?php echo $data['nm_banner']; ?></td>
        <td><a href="../image/baner/<?php echo  $data['img_banner'];?>" target="_blank"><img src="../image/baner/<?php echo  $data['img_banner'];?>" alt="<?php echo $data['img_banner'];?>" width="710px" height="200px"></a></td>
        <td>
          <button class="btn btn-warning" data-toggle="modal" data-target="#ConfEditBnr" data-id="<?php echo $data['id_banner'];?>"><i class="fa fa-edit"></i></button>
          <button class="btn btn-danger" data-toggle="modal" data-target="#confHapusBnr" data-href="baner/hapus_baner.php?id=<?php echo $data['id_banner'];?>"><i class="fa fa-trash-o"></i></button>
        </td>
      </tr>
      <?php 
        };
      ?>
    </tbody>
  </table>
  </div>
</div>

      <?php      
        if (isset($_GET['m'])) {
          if ($_GET['m'] === '1') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Diubah!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '0') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Diubah!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] === '00') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Dihapus!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] === '110') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Dihapus!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '10') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Ditambahkan!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '100') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Ditambahkan!", "", "warning");
                </script>';
          }elseif ($_GET['m'] === '0000') {
            echo'<script type="text/javascript">
                  swal("Ukuran image  harus 1140x380px!", "", "warning");
                </script>';
          }elseif ($_GET['m'] === '000') {
            echo'<script type="text/javascript">
                  swal("File exist!", "", "warning");
                </script>';
          }
        }
       ?>
  <!-- Modal Hapus -->
      <div class="modal fade" id="confHapusBnr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i><strong> Yakin Hapus?</strong></h4>
              <p class="debug-url"></p>
            </div>
            <div class="modal-footer">
              <a class="btn btn-danger btn-ok" href=bahan/hapus_bahan.php?id=<?php echo $data['id_bahan'];?>"><i class="fa fa-fw fa-power-off"></i> Ya</a>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal hapus-->

<script type="text/javascript">

$('#confHapusBnr').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
$(document).ready(function(){
    $('#ConfEditBnr').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
            url : 'baner/fetch_record.php', //Here you will fetch records 
            data :  'id='+ rowid, //Pass $id
            success : function(data){
            $('.fetched-data').html(data);//Show fetched data from database
            }
        });
     });
});
</script>


<!-- Modal Edit-->
  <div class="modal fade" id="ConfEditBnr" role="dialog" aria-hidden="true">

    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="fa fa-image"></span> Data Baner</h3>
        </div>
        <div class="modal-body">
          <div class="fetched-data">
          </div>
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal edit  -->

<!-- Modal Add-->
  <div class="modal fade" id="Add" role="dialog" aria-hidden="true">

    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="fa fa-image"></span> Data Baner</h3>
        </div>
        <div class="modal-body">
            <form role="form" action="baner/pro_add_baner.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                            <label><span><i class="fa fa-list"></i> Nama Baner</span></label>
                            <!-- <input type="hidden" name="id" class="form-control" value="'.$id_kota.'"> -->
                            <input type="text" class="form-control" name="nm_baner" placeholder="Nama baner.." required>
                </div>
                <div class="form-group">
                  <label><span><i class="fa fa-image"></i> Pilih Gambar</span></label>
                  <input type="file" name="img_baner" placeholder="Pilih Gambar (1140x380 pixels)" required>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block" name="submit"><span class="glyphicon glyphicon-off"></span> Add</button>
                </div>  
          </form>
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal add  -->