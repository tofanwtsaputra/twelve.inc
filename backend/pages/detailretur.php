<?php 
if (empty($_SESSION['user']==1)) 
{
  header('location:../../index.php');
}

$nota = $_GET['id'];

        include'../koneksi.php';
 ?>
<ol class="breadcrumb">
  <li>
    <i class="fa fa-dashboard"></i>  <a href="index.php?p=dashboard">Dashboard</a>
  </li>                            
  <li class="active">
    <i class="fa fa-fw fa-folder-open"></i> Transaksi
  </li>
  <li class="active">
    <i class="fa fa-table"></i> Retur - Detail
  </li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading">
    <strong><i class="fa fa-table"></i>  No Order : <?php echo $nota; ?>
      <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#confdetor" data-href="retur/konfirmasi_retur.php?id=<?php echo $nota;?>" title="Konfirmasi Order Ini"><i class="fa fa-check"></i></button>
    </strong>
  </div>
  <div class="panel-body">
    <?php 
      $qcust = "SELECT no_nota ,tgl_retur, jml_retur, ket_retur, status, nm_lengkap, alamat, kode_pos, email, telepon, nm_kota 
                    FROM retur
                      INNER JOIN customer ON (retur.id_cust = customer.id_cust)
                      INNER JOIN kota ON (customer.id_kota = kota.id_kota)
                      WHERE no_nota = $nota";
        $result = mysqli_query($conn, $qcust);
        $dcust = mysqli_fetch_array($result);
        extract($dcust);
     ?>
    <div class="panel panel-default">
       <div class="panel-heading"><strong><i class="fa fa-user"></i>  Detail Customer</strong></div>
       <div class="panel-body">
        <table class="table table-bordered">
          <th>Nama Customer</th>
          <th>Alamat Tujuan Pengiriman Produk</th>
          <th>Kota</th>
          <tr>
            <td>
                <?php echo ($nm_lengkap); ?>
            </td>
            <td>
                <?php echo($alamat);?>
            </td>
            <td>
              <?php echo($nm_kota); ?>
            </td>
          </tr>            
        </table>        
       </div><!--end panel def  -->
    </div><!-- end panel def -->

    <div class="panel panel-default">
      <div class="panel-heading"><strong><i class="fa fa-table"></i> Retur</strong></div>
      <div class="panel-body">        
        <table class="table table-responsive table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>Nomor Nota</th>
              <th>Nama Customer</th>
              <th>Jumlah Barang</th>
              <th>Tgl Retur</th>
              <th>Keterangan</th>
              <th>Status</th>
            </tr>
          </thead>
        <tbody>
          <tr>
            <td><?php echo $no_nota; ?></td>
            <td><?php echo $nm_lengkap; ?></td>
            <td><?php echo $jml_retur; ?></td>
            <td><?php echo $tgl_retur; ?></td>
            <td><?php echo $ket_retur; ?></td>
            <td><?php echo $status; ?></td>
            <!-- <td> -->
           <!--  <a class="btn btn-success" href="index.php?p=detailorder&id=<?php //echo $no_nota;?>" title="Lihat Detail order"><i class="fa fa-search-plus"></i></a> -->
              <!--<button class="btn btn-warning" data-toggle="modal" data-target="#ConfEditSab" data-id="<?php// echo $data['no_nota'];?>"></button> -->
              <!-- <button class="btn btn-danger" data-toggle="modal" data-target="#confHapusSab" data-href="sablon/hapus_sablon.php?id=<?php //echo $no_nota;?>"><i class="fa fa-trash-o"></i></button>
            </td> -->
          </tr>
          </tbody>
        </table>
      </div><!-- end panel body -->
    </div><!-- end panel def -->

    <div class="panel panel-default">
      <div class="panel-heading"><strong><i class="fa fa-list"></i>  Detail Retur Produk</strong></div>
      <div class="panel-body">
        
        <table class="table table-responsive table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Ukuran</th>
              <th>Jumlah Item</th>
            </tr>
          </thead>
          <?php 
              $query = "SELECT id_retur_det, no_nota, retur_detail.id_produk, retur_detail.id_ukuran, qty_retur, nm_produk, nm_ukuran
                          FROM retur_detail
                            INNER JOIN produk ON (retur_detail.id_produk = produk.id_produk)
                            INNER JOIN ukuran ON (retur_detail.id_ukuran = ukuran.id_ukuran)
                            WHERE no_nota = '$nota'";
              $result = mysqli_query($conn, $query);
              
              $i = 0;
           ?>
          <tbody>
          <?php 
            while ($data = mysqli_fetch_array($result)) 
            {
                $i++;
           ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo $data['nm_produk']; ?></td>
              <td><?php echo $data['nm_ukuran']; ?></td>
              <td><?php echo $data['qty_retur']; ?></td>
            </tr>
            <?php 
              };
            ?>
          </tbody>
        </table>
      </div>
    </div><!-- end panel def -->


  </div><!-- end panel body -->
</div><!-- end panel def -->

      <?php      
        if (isset($_GET['m'])) {
          if ($_GET['m'] == 1) {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Diubah!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] == 0) {
            echo'<script type="text/javascript">
                  swal("Data Gagal Diubah!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] == 00) {
            echo'<script type="text/javascript">
                  swal("Data Gagal Dihapus!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] == 01) {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Dihapus!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] == 10) {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Ditambahkan!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '11') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Ditambahkan!", "", "warning");
                </script>';
          }
        }
       ?>
  <!-- Modal konfirmasi -->
      <div class="modal fade" id="confdetor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i><strong> Yakin Konfirmasi Retur ini?</strong></h4>
              <p class="debug-url"></p>
            </div>
            <div class="modal-footer">
              <a class="btn btn-primary btn-ok" href="retur/konfirmasi_retur.php?id=<?php echo $no_nota;?>"><i class="fa fa-fw fa-check"></i> Ya</a>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal konfirmasi-->