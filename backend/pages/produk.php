<?php 
if (empty($_SESSION['user']==1)) 
{
  header('location:../../index.php');
}
 ?>
                <?php 
                    include'../koneksi.php';
                    $query = "SELECT id_produk, kd_produk, nm_produk, desk_produk, image, produk.id_sablon, nm_sablon, produk.id_bahan, nm_bahan, harga_jual, harga_beli,produk.kd_kategori, nm_kategori, tgl_masuk, balance, disc, berat
                                FROM produk 
                                INNER JOIN kategori ON (produk.kd_kategori)=(kategori.kd_kategori)
                                INNER JOIN sablon ON (produk.id_sablon)=(sablon.id_sablon)
                                INNER JOIN bahan ON (produk.id_bahan)=(bahan.id_bahan)";
                                //print_r($query);die();
                    $result = mysqli_query($conn, $query);
                    
                    $i = 0;
                 ?>
<ol class="breadcrumb">
  <li>
    <i class="fa fa-dashboard"></i>  <a href="index.php?p=dashboard">Dashboard</a>
  </li>                            
  <li class="active">
    <i class="fa fa-fw fa-folder-open"></i> Master
  </li>
  <li class="active">
    <i class="fa fa-th"></i> Produk
  </li>
</ol>     
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong><i class="fa fa-table"></i>  Tabel Produk</strong>
        </div>
        <div class="panel-body">
          <a href="#ConfAdd" data-toggle="modal"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Baru</button></a>  
            <div class="table-responsive"><br>      
                <table class="table table-responsive table-hover table-striped table-bordered example">
                  <thead>
                    <tr>
                      <!-- <th>Id</th> -->
                      <th>Kd</th>
                      <th>Nama</th>
                      <th>Desc</th>
                      <th>Gambar</th>
                      <th>Sablon</th>
                      <th>Bhn</th>
                      <th>$Jual</th>
                      <th>$Beli</th>
                      <th>Kategori</th>
                      <th>Tgl Msk</th>
                      <th>Stok</th>
                      <th>Disc(%)</th>
                      <th>Berat(kg)</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      while ($data = mysqli_fetch_array($result)) 
                      {
                          $i++;
                     ?>
       
                    <tr>
                      <!-- <td><?php //echo $data['id_produk']; ?></td> -->
                      <td><?php echo $data['kd_produk']; ?></td>
                      <td><?php echo $data['nm_produk']; ?></td>
                      <td><?php echo $data['desk_produk']; ?></td>
                      <td><a href="../image/produk/<?php echo  $data['image'];?>" target="_blank"><img src="../image/produk/<?php echo  $data['image'];?>" alt="<?php echo $data['image'];?>" width="128" height="128"></a></td>
                      <td><?php echo $data['nm_sablon']; ?></td>
                      <td><?php echo $data['nm_bahan']; ?></td>
                      <td>Rp <?php echo $dt=number_format($data['harga_jual']);?></td>
                      <td>Rp <?php echo $dt=number_format($data['harga_beli']);?></td>
                      <td><?php echo $data['nm_kategori']; ?></td>
                      <td><?php echo $data['tgl_masuk']; ?></td>
                      <td><?php echo $data['balance']; ?></td>
                      <!-- <td><?php //echo $data['min_stok']; ?></td> -->
                      <!-- <td><?php //echo $data['max_stok']; ?></td> -->
                      <td><?php echo $data['disc']; ?></td>
                      <td><?php echo $data['berat']; ?></td>
                      <td>
                        <button class="btn btn-warning" data-toggle="modal" data-target="#ConfEdit" data-id="<?php echo $data['id_produk'];?>"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#confHapus" data-href="produk/hapus_produk.php?id=<?php echo $data['id_produk'];?>"><i class="fa fa-trash-o"></i></button>
                      </td>
                    </tr>

                    <?php 
                      }
                     ?>
                  </tbody>
                </table>
        </div>
      </div>

</div>

                <?php
        if (isset($_GET['m'])) {
          if ($_GET['m'] === '000') {
            echo'<script type="text/javascript">
                  swal("Maaf, Kode Produk telah terdaftar di Database.", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '001') {
            echo'<script type="text/javascript">
                  swal("Maaf,Ukuran File Gambar Terlalu Besar!", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '010') {
            echo'<script type="text/javascript">
                  swal("Maaf, hanya File JPG, JPEG, PNG yang Diperbolehkan!", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '011') {
            echo'<script type="text/javascript">
                  swal("Maaf, File Tidak Berhasil diupload !", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '111') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Ditambahkan!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '100') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Ditambahkan!", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '110') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Dihapus!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '101') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Dihapus!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] === '1111') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Diupdate!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '1000') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Diupdate!", "", "danger");
                </script>';
          }
        } 
                 ?>


      <!-- Modal Hapus -->
      <div class="modal fade" id="confHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i><strong> Yakin Hapus?</strong></h4>
              <p class="debug-url"></p>
            </div>
            <div class="modal-footer">
              <a class="btn btn-danger btn-ok" href="produk/hapus_produk.php?id=<?php echo $data['id_produk'];?>"><i class="fa fa-fw fa-power-off"></i> Ya</a>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
            </div>
          </div>
        </div>
      </div>
<script type="text/javascript">

$('#confHapus').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('#ConfEdit').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        $.ajax({
            type : 'post',
            url : 'produk/fetch_record.php', //Here you will fetch records 
            data :  'id='+ rowid, //Pass $id
            success : function(data){
            $('.fetched-data').html(data);//Show fetched data from database
            }
        });
     });
});
</script>



<!-- Modal Edit-->
  <div class="modal fade" id="ConfEdit" role="dialog" aria-hidden="true">

    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="fa fa-group"></span> Data Produk</h3>
        </div>
        <div class="modal-body">
          <div class="fetched-data">
          </div>
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal edit  -->


<!-- Modal Add-->
  <div class="modal fade" id="ConfAdd" role="dialog" aria-hidden="true">

    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="fa fa-group"></span> Data Produk</h3>
        </div>
        <div class="modal-body">
          <form role="form" action="produk/pro_add_produk.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Kode Produk</span></label>
              <input type="text" class="form-control" name="kd_produk" placeholder="Kode Produk" required value="">
            </div>  
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Nama Produk</span></label>
              <input type="text" class="form-control" name="nm_produk" placeholder="Nama Produk" required value="">
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-home"></i> Deskripsi</span></label>
              <textarea class="form-control" name="desk_produk" required placeholder="Deskripsi"></textarea>
            </div>      
            <div class="form-group">
              <label><span><i class="fa fa-image"></i>Pilih Gambar</span></label>
              <input type="file" name="image" placeholder="pilih gambar" required>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Sablon</span></label>
              <select name="sablon" class="form-control" value="">
                <?php 
                $q_sablon = "SELECT id_sablon, nm_sablon FROM sablon";
                $res = mysqli_query($conn,$q_sablon);
                while ($kat = mysqli_fetch_array($res)) {
                  extract($kat);
                  echo'<option value="'.$id_sablon.'">'.$nm_sablon.'</option>';
                }
               ?>
              </select>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Bahan</span></label>
              <select name="bahan" class="form-control" value="">
                <?php 
                $q_bahan = "SELECT id_bahan, nm_bahan FROM bahan";
                $res = mysqli_query($conn,$q_bahan);
                while ($kat = mysqli_fetch_array($res)) {
                  extract($kat);
                  echo'<option value="'.$id_bahan.'">'.$nm_bahan.'</option>';
                }
               ?>
              </select>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-circle "></i> Harga Jual</span></label>
              <input type="text" class="form-control" name="hrg_jual"  placeholder="Harga Jual"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required value="">
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Harga Beli</span></label>
              <input type="text" class="form-control" name="hrg_beli"  placeholder="Harga Beli"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required value="">
            </div>                       
            <div class="form-group">
              <label><span><i class="fa fa-globe"></i> Kategori</span></label>
              <select name="kd_kategori" class="form-control" value="">
              <?php 
                $q_kategori = "SELECT kd_kategori, nm_kategori FROM kategori";
                $res = mysqli_query($conn,$q_kategori);
                while ($kat = mysqli_fetch_array($res)) {
                  extract($kat);
                  echo'<option value="'.$kd_kategori.'">'.$nm_kategori.'</option>';
                }
               ?>
              </select>
            </div>
            <div class="form-group">
              <label><span><i class="fa fa-user"></i> Tanggal Masuk</span></label>
              <input type="text" class="form-control" name="tgl_msk" placeholder="Tanggal Masuk" required value="<?php echo $t=date('Y-m-d'); ?>">
            </div>  
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Stok</span></label>
              <input type="text" class="form-control" name="stok"  placeholder="Jumlah Stok"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required value="">
            </div> 
<!--             <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Stok Minimal</span></label>
              <input type="text" class="form-control" name="min_stok"  placeholder="Jumlah stok minimal"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required value="">
            </div> 
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Stok Maksimal</span></label>
              <input type="text" class="form-control" name="max_stok"  placeholder="Jumlah stok maksimal"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required value="">
            </div> --> 
            <div class="form-group">
              <label><span><i class="fa fa-phone"></i> Berat</span></label>
              <input type="text" class="form-control" name="berat"  placeholder="Berat Produk"
               onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
               onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required value="">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" name="submit"><span class="glyphicon glyphicon-off"></span> Add</button>
            </div>  
          </form>
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal add  -->
