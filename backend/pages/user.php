<?php 
if (empty($_SESSION['user']==1)) 
{
  header('location:../../index.php');
}
 ?>
<ol class="breadcrumb">
  <li>
    <i class="fa fa-dashboard"></i>  <a href="index.php?p=dashboard">Dashboard</a>
  </li>                            
  <li class="active">
    <i class="fa fa-fw fa-folder-open"></i> Master
  </li>
  <li class="active">
    <i class="fa fa-user"></i> User
  </li>
</ol>

<div class="panel panel-default">
  <div class="panel-heading"><strong><i class="fa fa-table"></i>  Tabel User</strong></div>
  <div class="panel-body"> 

  <a href="index.php?p=add"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Baru</button></a>
    <br> <br>     
  <table class="table table-striped table-hover table-bordered example">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nick</th>
        <th>Username</th>
        <th>Level</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <?php 
        include'../koneksi.php';
        $query = "SELECT *
                    FROM admin";
        $result = mysqli_query($conn, $query);
        
        $i = 0;
     ?>
    <tbody>
    <?php 
      while ($data = mysqli_fetch_array($result)) 
      {
          $i++;
     ?>
      <tr>
        <td><?php echo $data['id_usr']; ?></td>
        <td><?php echo $data['nama']; ?></td>
        <td><?php echo $data['email']; ?></td>
        <td><?php echo $data['level']; ?></td>
        <td>
          <button class="btn btn-warning" data-toggle="modal" data-target="#ConfEditKatser" data-id="<?php echo $data['id_usr'];?>"><i class="fa fa-edit"></i></button>
          <button class="btn btn-danger" data-toggle="modal" data-target="#confHapusUser" data-href="user/hapus_user.php?id=<?php echo $data['id_usr'];?>"><i class="fa fa-trash-o"></i></button>
        </td>
      </tr>
      <?php 
        };
       ?>
    </tbody>
  </table>
  </div>
</div>
  