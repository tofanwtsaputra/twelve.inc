<?php 
if (empty($_SESSION['user']==1)) 
{
  header('location:../../index.php');
}

$nota = $_GET['id'];

        include'../koneksi.php';
 ?>
<ol class="breadcrumb">
  <li>
    <i class="fa fa-dashboard"></i>  <a href="index.php?p=dashboard">Dashboard</a>
  </li>                            
  <li class="active">
    <i class="fa fa-fw fa-folder-open"></i> Transaksi
  </li>
  <li class="active">
    <i class="fa fa-table"></i> Orders - Detail
  </li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading">
    <strong><i class="fa fa-table"></i>  No Order : <?php echo $nota; ?>
      <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#confdetor" data-href="orders/konfirmasi_orders.php?id=<?php echo $nota;?>" title="Konfirmasi Order Ini"><i class="fa fa-check"></i></button>
    </strong>
  </div>
  <div class="panel-body">
    <?php 
      $qcust = "SELECT no_nota ,tgl_order, jumlah, grand_total, status, nm_lengkap, alamat, kode_pos, email, telepon, nm_kota 
                    FROM orders
                      INNER JOIN customer ON (orders.id_cust = customer.id_cust)
                      INNER JOIN kota ON (customer.id_kota = kota.id_kota)
                      WHERE no_nota = $nota";
        $result = mysqli_query($conn, $qcust);
        $dcust = mysqli_fetch_array($result);
        extract($dcust);
     ?>
    <div class="panel panel-default">
       <div class="panel-heading"><strong><i class="fa fa-user"></i>  Detail Customer</strong></div>
       <div class="panel-body">
        <table class="table table-bordered">
          <th>Nama Customer</th>
          <th>Alamat Tujuan Pengiriman Produk</th>
          <th>Kota</th>
          <tr>
            <td>
                <?php echo ($nm_lengkap); ?>
            </td>
            <td>
                <?php echo($alamat);?>
            </td>
            <td>
              <?php echo($nm_kota); ?>
            </td>
          </tr>            
        </table>        
       </div><!--end panel def  -->
    </div><!-- end panel def -->

    <div class="panel panel-default">
      <div class="panel-heading"><strong><i class="fa fa-table"></i> Orders</strong></div>
      <div class="panel-body">        
        <table class="table table-responsive table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>Nomor Nota</th>
              <th>Nama Customer</th>
              <th>Jumlah Barang</th>
              <th>Grand Total</th>
              <th>Tgl Order</th>
              <th>Status</th>
            </tr>
          </thead>
        <tbody>
          <tr>
            <td><?php echo $no_nota; ?></td>
            <td><?php echo $nm_lengkap; ?></td>
            <td><?php echo $jumlah; ?></td>
            <td><?php echo 'Rp '.number_format($grand_total); ?></td>
            <td><?php echo $tgl_order; ?></td>
            <td><?php echo $status; ?></td>
            <!-- <td> -->
           <!--  <a class="btn btn-success" href="index.php?p=detailorder&id=<?php //echo $no_nota;?>" title="Lihat Detail order"><i class="fa fa-search-plus"></i></a> -->
              <!--<button class="btn btn-warning" data-toggle="modal" data-target="#ConfEditSab" data-id="<?php// echo $data['no_nota'];?>"></button> -->
              <!-- <button class="btn btn-danger" data-toggle="modal" data-target="#confHapusSab" data-href="sablon/hapus_sablon.php?id=<?php //echo $no_nota;?>"><i class="fa fa-trash-o"></i></button>
            </td> -->
          </tr>
          </tbody>
        </table>
      </div><!-- end panel body -->
    </div><!-- end panel def -->

    <div class="panel panel-default">
      <div class="panel-heading"><strong><i class="fa fa-list"></i>  Detail Order Produk</strong></div>
      <div class="panel-body">
        
        <table class="table table-responsive table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Ukuran</th>
              <th>Jumlah Item</th>
              <th>Harga</th>
              <th>Sub Total</th>
            </tr>
          </thead>
          <?php 
              $query = "SELECT id_order_det, no_nota, order_detail.id_produk, order_detail.id_ukuran, qty_order, sub_total, nm_produk, harga_jual, nm_ukuran
                          FROM order_detail
                            INNER JOIN produk ON (order_detail.id_produk = produk.id_produk)
                            INNER JOIN ukuran ON (order_detail.id_ukuran = ukuran.id_ukuran)
                            WHERE no_nota = '$nota'";
              $result = mysqli_query($conn, $query);
              
              $i = 0;
           ?>
          <tbody>
          <?php 
            while ($data = mysqli_fetch_array($result)) 
            {
                $i++;
           ?>
            <tr>
              <td><?php echo $i; ?></td>
              <td><?php echo $data['nm_produk']; ?></td>
              <td><?php echo $data['nm_ukuran']; ?></td>
              <td><?php echo $data['qty_order']; ?></td>
              <td><?php echo 'Rp '.number_format($data['harga_jual']); ?></td>
              <td><?php echo 'Rp '.number_format($data['sub_total']); ?></td>
            </tr>
            <?php 
              };
            ?>
          </tbody>
        </table>
      </div>
    </div><!-- end panel def -->

    <div class="panel panel-default">
      <div class="panel-heading"><strong><i class="fa fa-list"></i>  Detail Konfirmasi Pembayaran</strong></div>
      <div class="panel-body">
        
        <table class="table table-responsive table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>No Nota</th>
              <th>Nama Bank</th>
              <th>No Rekening Customer</th>
              <th>Atas Nama</th>
              <th>No Rekening Tujuan</th>
              <th>Bukti Pembayaran</th>
            </tr>
          </thead>
          <?php 
              $query = "SELECT id_konfirm, no_nota, konfirmasi.nm_bank, no_rek, a_nama, tujuan, bukti, bank.nm_bank as nm_bank_tujuan , no_rekening, pemilik
                          FROM konfirmasi
                            INNER JOIN bank ON (konfirmasi.tujuan = bank.no_rekening)
                            WHERE no_nota = '$nota'";
                //print_r($query);die();
              $result = mysqli_query($conn, $query);
              
              $i = 0;
           ?>
          <tbody>
          <?php 
            while ($data = mysqli_fetch_array($result)) 
            {
                $i++;
           ?>
            <tr>
              <td><?php echo $data['no_nota']; ?></td>
              <td><?php echo $data['nm_bank']; ?></td>
              <td><?php echo $data['no_rek']; ?></td>
              <td><?php echo $data['a_nama']; ?></td>
              <td><?php echo $data['nm_bank_tujuan'].' - '.$data['tujuan'].' - '.$data['pemilik']; ?></td>
              <td><a href="../image/konfirmasi/<?php echo  $data['bukti'];?>" target="_blank"><img src="../image/konfirmasi/<?php echo  $data['bukti'];?>" alt="<?php echo $data['bukti'];?>" width="128" height="128"></a></td>
            </tr>
            <?php 
              };
            ?>
          </tbody>
        </table>
      </div>
    </div><!-- end panel def -->

  </div><!-- end panel body -->
</div><!-- end panel def -->

      <?php      
        if (isset($_GET['m'])) {
          if ($_GET['m'] == 1) {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Diubah!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] == 0) {
            echo'<script type="text/javascript">
                  swal("Data Gagal Diubah!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] == 00) {
            echo'<script type="text/javascript">
                  swal("Data Gagal Dihapus!", "", "danger");
                </script>';
          }
          elseif ($_GET['m'] == 01) {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Dihapus!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] == 10) {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Ditambahkan!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '11') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Ditambahkan!", "", "warning");
                </script>';
          }
        }
       ?>
  <!-- Modal konfirmasi -->
      <div class="modal fade" id="confdetor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i><strong> Yakin Konfirmasi Order ini?</strong></h4>
              <p class="debug-url"></p>
            </div>
            <div class="modal-footer">
              <a class="btn btn-primary btn-ok" href="orders/konfirmasi_orders.php?id=<?php echo $no_nota;?>"><i class="fa fa-fw fa-check"></i> Ya</a>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal konfirmasi-->