<?php 
if (empty($_SESSION['user']==1)) 
{
  header('location:../../index.php');
}
 ?>
<ol class="breadcrumb">
  <li>
    <i class="fa fa-dashboard"></i>  <a href="index.php?p=dashboard">Dashboard</a>
  </li>                            
  <li class="active">
    <i class="fa fa-fw fa-folder-open"></i> Transaksi
  </li>
  <li class="active">
    <i class="fa fa-undo"></i> Retur
  </li>
</ol>
<div class="panel panel-default">
  <div class="panel-heading"><strong><i class="fa fa-table"></i>  Tabel Retur</strong></div>
  <div class="panel-body">        
  <table class="table table-responsive table-hover table-striped table-bordered example">
    <thead>
      <tr>
        <th>No</th>
        <th>Nomor Nota</th>
        <th>Nama Customer</th>
        <th>Jumlah Barang</th>
        <th>Tanggal Retur</th>
        <th>Keterangan</th>
        <th>Status</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <?php 
        include'../koneksi.php';
        $query = "SELECT no_nota, tgl_retur, jml_retur,ket_retur ,nm_lengkap, status
                    FROM retur
                      INNER JOIN customer ON (retur.id_cust = customer.id_cust)";
        $result = mysqli_query($conn, $query);
        
        $i = 0;
     ?>
    <tbody>
    <?php 
      while ($data = mysqli_fetch_array($result)) 
      {
          $i++;
     ?>
      <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $data['no_nota']; ?></td>
        <td><?php echo $data['nm_lengkap']; ?></td>
        <td><?php echo $data['jml_retur']; ?></td>
        <td><?php echo $data['tgl_retur']; ?></td>
        <td><?php echo $data['ket_retur']; ?></td>
        <td><?php echo $data['status']; ?></td>
        <td>
        <a class="btn btn-info" href="index.php?p=detailretur&id=<?php echo $data['no_nota'];?>" title="Lihat Detail order"><i class="fa fa-search-plus"></i></a>
<!--           <button class="btn btn-warning" data-toggle="modal" data-target="#ConfEditSab" data-id="<?php// echo $data['no_nota'];?>"></button> -->
          <button class="btn btn-danger" data-toggle="modal" data-target="#confHapusRet" data-href="retur/hapus_retur.php?id=<?php echo $data['no_nota'];?>"><i class="fa fa-trash-o"></i></button>
        </td>
      </tr>
      <?php 
        };
      ?>
    </tbody>
  </table>
  </div>
</div>

      <?php      
        if (isset($_GET['m'])) {
          if ($_GET['m'] === '1') {
            echo'<script type="text/javascript">
                  swal("Order Berhasil Dikonfirmasi!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '0') {
            echo'<script type="text/javascript">
                  swal("Order Gagal Dikonfirmasi!", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '00') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Dihapus!", "", "warning");
                </script>';
          }
          elseif ($_GET['m'] === '01') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Dihapus!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '10') {
            echo'<script type="text/javascript">
                  swal("Data Berhasil Ditambahkan!", "", "success");
                </script>';
          }
          elseif ($_GET['m'] === '11') {
            echo'<script type="text/javascript">
                  swal("Data Gagal Ditambahkan!", "", "warning");
                </script>';
          }
        }
       ?>
  <!-- Modal Hapus -->
      <div class="modal fade" id="confHapusRet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i><strong> Yakin Hapus?</strong></h4>
              <p class="debug-url"></p>
            </div>
            <div class="modal-footer">
              <a class="btn btn-danger btn-ok" href="orders/hapus_orders.php?id=<?php $data['no_nota'];?>"><i class="fa fa-fw fa-power-off"></i> Ya</a>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal hapus-->

<script type="text/javascript">

$('#confHapusRet').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
</script>