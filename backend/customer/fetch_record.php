<?php
include'../../koneksi.php';
if($_POST['id']) {
    $id = $_POST['id']; //escape string
    $query = "SELECT * FROM customer WHERE id_cust=$id";
    //print_r($query);
    $result = mysqli_query($conn, $query);
    $data_cust = mysqli_fetch_array($result);
    extract($data_cust);
    $data='
            <form role="form" action="customer/pro_edit_customer.php" method="post">
                <div class="form-group">
                            <label><span><i class="fa fa-user nama"></i> Nama Lengkap</span></label>
                            <input type="hidden" name="id" class="form-control" value="'.$id_cust.'">
                            <input type="text" class="form-control" id="nm_lengkap" name="nm_lengkap" placeholder="Nama Lengkap" required value="'.$nm_lengkap.'">
                </div>  
                <div class="form-group">
                    <label><span><i class="fa fa-home"></i> Alamat Lengkap</span></label>
                    <textarea class="form-control" name="alamat" id="alamat" required placeholder="Alamat Lengkap">'.$alamat.'</textarea>
                </div>  
                <div class="form-group">
                    <label><span><i class="fa fa-circle "></i> Kode Pos</span></label>
                    <input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode Pos"
                              required value="'.$kode_pos.'">
                </div>
                <div class="form-group">
                    <label><span><i class="fa fa-phone"></i> No Telepon</span></label>
                    <input type="text" class="form-control" name="telepon" id="telepon" placeholder="No Telepon"
                              required value="'.$telepon.'">
                </div>                       
                <div class="form-group">
                    <label><span><i class="fa fa-globe"></i> Kota</span></label>
 						<select name="kota" class="form-control" value="'.$id_kota.'">';
                            $q_kota = "SELECT id_kota, nm_kota FROM kota";
                            $res = mysqli_query($conn,$q_kota);
                            while($kt = mysqli_fetch_array($res)){ 
                            $id_k = $kt['id_kota'];
                            $nm_ko = $kt['nm_kota']; 
                            $data.='<option value="'.$id_k.'"';
                            $k = ($id_kota==$id_k) ? $s='selected' : $s='' ;
                            $data.=$s;
                            $data.='>'.$nm_ko.'</option>';}
                            $data.='</select>
                </div>  
                <div class="form-group">
                    <label><span><i class="fa fa-envelope"></i> Email</span></label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required value="'.$email.'">  
                </div> 
                <div class="form-group">
                    <button type="submit" class="btn btn-warning btn-block"><span class="glyphicon glyphicon-off"></span> Update</button>
                </div>  
            </form>


    ';
    echo $data;
    // Fetch Records
    // Echo the data you want to show in modal
 }
?>