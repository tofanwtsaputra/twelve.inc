<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
    <link rel="icon" href="image/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap -->
    <!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/bootstrap-social.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="bootstrap/css/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/dataTables.bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="bootstrap/css/jPages.css">


    <!-- <script src="bootstrap/js/bootstrap-scroll-modal.js"></script> -->
 <!-- <script type="text/javascript" src="bootstrap/js/jquery.dataTables.min.js"></script> -->
</head>
<body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="./"><span class="fa fa-home"></span> Twelve Inc</a>
                </div>


                <div class="collapse navbar-collapse pull-right">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="index.php?p=keranjangbelanja"><i class="fa fa-shopping-cart"></i> <span class="badge">
                            <?php
                                include 'koneksi.php';
                                $sid = session_id();
                                $qkb = "SELECT count(*) FROM order_tmp WHERE session_order = '$sid'  ";
                                //print_r($qkb);die();
                                $result = mysqli_query($conn,$qkb);
                                $row = mysqli_fetch_array($result);
                                echo($row["count(*)"]);
                            ?></span></a>
                        </li>
                        <li>
                            <?php  
                                if (isset($_SESSION['customer'])) {
                                    $id = $_SESSION['customer'];
                                    $qnm = "SELECT * FROM customer WHERE id_cust=$id";
                                    $result = mysqli_query($conn,$qnm);
                                    $data = mysqli_fetch_array($result);
                                    echo"<a><i class='fa fa-user'></i> Halo ".$data['nm_lengkap']."</a>";
                                }
                                else{
                                    echo('<a href="#" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-user"></i> Masuk<!--  || <i class="fa fa-group"></i> Daftar --></a>');
                                }
                            ?>
                        </li>
                        <li><a href="#" data-toggle="modal" data-target="#modalKonfirm"><i class="fa  fa-check-circle"></i> Konfirmasi </a></li>
                        <?php 
                            if (isset($_SESSION['customer'])) {
                                echo '
                                    <li><a href="#" data-toggle="modal" data-target="#modalRetur"><i class="fa fa-question-circle"></i> Retur</a>
                                    </li>';
                            }
                         ?>
                        <li><a href="#" data-toggle="modal" data-target="#modalCrPesan"><i class="fa fa-question-circle"></i> Cara Pesan</a></li>
                        <li><a href="index.php?p=about"><i class="fa fa-info-circle"></i> About Us</a>
                        </li>
                        <li>
                            <?php 
                                if (isset($_SESSION['customer'])) {
                                    echo('
                                        <a href="#" data-toggle="modal" data-target="#modLogout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                        ');
                                }
                             ?>
                        </li>
                    </ul>

                </div> 
            </div><!-- container fluid-->

        </div>
        

<!-- Modal login-->
  <div class="modal fade in" id="modalLogin" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color: #333; color: #fff;">
          <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
          <center><h2><span class="fa fa-user"></span> Login</h2></center>
        </div>
        <div class="modal-body">
            <form role="form" action="login_proccess.php" method="post">
                <div class="form-group">
                  <label for="usrname"><span class="fa fa-envelope"></span> Email</label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" autofocus required>
                </div>
                <div class="form-group">
                  <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                  <input type="password" class="form-control" id="psw" name="password" placeholder="Enter password" required>
                </div>
                <div class="form-group">
                    <button type="submit" name="login" class="btn btn-block" style="background-color: #333; color: #fff"><span class="glyphicon glyphicon-off"></span> Login</button>
                </div>            
            </form>
        </div>
        <div class="modal-footer">            
            <span align="center">Belum Punya Akun? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalRegister"> Daftar</a></span>
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal login  -->

<!-- Modal Register-->
    <div class="modal fade in" id="modalRegister" role="dialog" aria-hidden="true">

        <div class="modal-dialog modal-md">
        
        <?php
            $digit1 = mt_rand(1,10);
            $digit2 = mt_rand(1,10);
            if( mt_rand(0,1) === 1 ) {
                    $math = "$digit1 + $digit2";
                    $_SESSION['answer'] = $digit1 + $digit2;
            } else {
                    $math = "$digit1 * $digit2";
                    $_SESSION['answer'] = $digit1 * $digit2;
            }
        ?>

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: #333; color: #fff">
              <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
              <h2><center><span class="fa fa-group"></span> Register</center></h2>
            </div>
            <div class="modal-body">
                <form role="form" name="register" action="daftar_proccess.php" method="post" id="register">
                    <div class="form-group">
                                <label><span><i class="fa fa-user"></i> Nama Lengkap</span></label>
                                <input type="text" class="form-control" id="nm_lengkap" name="nm_lengkap" placeholder="Nama Lengkap" required value="">
                    </div>  
                    <div class="form-group">
                        <label><span><i class="fa fa-home"></i> Alamat Lengkap</span></label>
                        <textarea class="form-control" name="alamat" id="alamat" required placeholder="Alamat Lengkap.. "></textarea>
                    </div> 
                    <div class="form-group">
                        <label><span><i class="fa fa-globe"></i> Kota</span></label>
                        <select name="kota" class="form-control" value="" id="kota">
                            <?php 
                                $qkota = "SELECT * FROM kota";
                                $result = mysqli_query($conn, $qkota);
                                while ($data = mysqli_fetch_array($result)) {
                                    echo('
                                        <option value="'.$data["id_kota"].'">'.$data["nm_kota"].'</option>
                                        ');
                                }
                              ?>
                        </select>
                    </div>     
                    <div class="form-group">
                        <label><span><i class="fa fa-envelope-o"></i> Kode Pos</span></label>
                        <input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode Pos"
                                 onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                                 onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                                  required value="">
                    </div>
                    <div class="form-group">
                        <label><span><i class="fa fa-phone"></i> No Telepon</span></label>
                        <input type="text" class="form-control" name="telepon" id="telepon" placeholder="No Telepon"
                                 onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                                 onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                                  required value="">
                    </div>  
                    <div class="form-group">
                        <label><span><i class="fa fa-envelope"></i> Email</span></label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required value="">
                    </div>    
                    <div class="form-group">
                        <label><span><i class="glyphicon glyphicon-eye-open"></i> Password</span></label>
                        <input type="password" class="form-control" name="pass" id="pass" placeholder="*****" required value="">
                    </div>   
                    <div class="form-group">
                        <label><span><i class="glyphicon glyphicon-eye-open"></i> Ketik Ulang Password </span></label>
                        <input type="password" class="form-control" name="repass" id="email" placeholder="*****" required value="">
                    </div>    
                    <div class="form-group">
                        <label><span class="fa fa-check"></span>
                         <?php 
                            echo 'Berapa Hasil Dari ' . $math . " = ?";
                         ?>
                        </label>
                        <input type="text" placeholder="Hasil" id="captcha" name="captcha" class="form-control" required
                                 onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                                 onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
                    </div>    
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block" name="daftar" style="background-color: #333; color: #fff"><span class="glyphicon glyphicon-off"></span> Register</button>
                    </div>  
                </form>
            </div>
            <div class="modal-footer">            
                <span align="center">Sudah Punya Akun? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalLogin"> Masuk</a></span>
            </div>
          </div>
          
        </div>
    </div> 
<!--end Modal Register  -->

<!-- Modal Konfirmasi-->
  <div class="modal fade in" id="modalKonfirm" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color: #333; color: #fff;">
          <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
          <center><h2><span class="fa  fa-check-circle"></span> Konfirmasi Pembayaran</h2></center>
        </div>
        <div class="modal-body">
            <form role="form" action="konfirmasi_proccess.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label><span><i class="fa fa-list"></i> No Nota</span></label>
                        <input type="text" class="form-control" name="no_nota" placeholder="No Nota" 
                        onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        required value="">
                </div> 
                <div class="form-group">
                  <label><span class="fa fa-money"></span> Nama Bank</label>
                  <input type="text" class="form-control" name="nm_bank" placeholder="BNI or BRI or ..." autofocus required>
                </div>
                <div class="form-group">
                    <label><span><i class="fa fa-list"></i> No Rekening</span></label>
                        <input type="text" class="form-control" name="no_rekening" placeholder="No Nota" 
                        onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        required value="">
                </div>
                <div class="form-group">
                    <label><span><i class="fa fa-user"></i> Atas Nama</span></label>
                        <input type="text" class="form-control" name="a_nama" placeholder="atas nama ..." required value="">
                </div>
                <div class="form-group">
                  <label><span><i class="fa fa-user"></i> Rekening Tujuan</span></label>
                  <select name="tujuan" class="form-control" value="">
                    <?php 
                    $q_bank = "SELECT * FROM bank";
                    $res = mysqli_query($conn,$q_bank);
                    while ($bank = mysqli_fetch_array($res)) {
                      extract($bank);
                      echo'<option value="'.$no_rekening.'">'.$nm_bank.' - '.$no_rekening.' - '.$pemilik.'</option>';
                    }
                   ?>
                  </select>
                </div>
                <div class="form-group">
                  <label><span><i class="glyphicon glyphicon-picture"></i> Bukti Pembayaran</span></label>
                  <input type="file" name="image" placeholder="pilih gambar" required>
                </div>
                <div class="form-group">
                    <button type="submit" name="konfirmasi" class="btn btn-block" style="background-color: #333; color: #fff"><span class="glyphicon glyphicon-off"></span> Konfirmasi</button>
                </div>            
            </form>
        </div>
        <div class="modal-footer">            
            <!-- <span align="center">Belum Punya Akun? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalRegister"> Daftar</a></span> -->
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal konfirmasi -->

<!-- Modal Retur-->
  <div class="modal fade in" id="modalRetur" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background-color: #333; color: #fff;">
          <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
          <center><h2><span class="fa  fa-check-circle"></span> Retur Produk</h2></center>
        </div>
        <div class="modal-body">
            <form role="form" action="retur_proccess.php" method="post">
                <div class="form-group">
                    <label><span><i class="fa fa-list"></i> No Nota</span></label>
                        <input type="text" class="form-control" name="no_nota" placeholder="No Nota" 
                        onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        required value="">
                </div> 
                <div class="form-group">
                  <label><span class="fa fa-money"></span> Keterangan</label>
                  <input type="text" class="form-control" name="keterangan" placeholder="Rusak ..." autofocus required>
                </div>
                <div class="form-group">
                    <button type="submit" name="retur" class="btn btn-block" style="background-color: #333; color: #fff"><span class="glyphicon glyphicon-off"></span> Retur</button>
                </div>            
            </form>
        </div>
        <div class="modal-footer">            
            <!-- <span align="center">Belum Punya Akun? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalRegister"> Daftar</a></span> -->
        </div>
      </div>
      
    </div>
  </div> 
<!--end Modal Retur -->

<!-- Modal Cara Pesan-->
    <div class="modal fade in" id="modalCrPesan" role="dialog" aria-hidden="true">

        <div class="modal-dialog modal-md">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: #333; color: #fff">
              <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
              <h2><center><span class="fa fa-question-circle"></span> Cara Pemesanan</center></h2>
            </div>
            <div class="modal-body">
                <p>1. Silahkan Login terlebih dahulu jika akan memesan produk dengan cara klik menu <code>Masuk</code> pada Menu Bar.</p>
                <p>2. Jika belum mempunyai akun, silahkan Register terlebih dulu dengan cara menklik link <code>Daftar</code> pada bagian bawah menu <code>Masuk</code>.</p>
                <p>3. Pilih Produk yang akan dibeli.</p>
                <p>4. Pilih Ukuran dan Jumlah item yang akan dibeli.</p>
                <p>5. Klik Tombol <code>Check - Out</code> pada halaman <code>Keranjang Belanja</code> jika sudah selesai memilih Produk</p>
                <p>6. Masukkan Alamat Lengkap, Kota dan No Hp yang bisa dihubungi.</p>
                <p>7. Klik Tombol <code>Kirim</code>.</p>
                <p>8. Selesai.</p>
            </div>
          </div>
          
        </div>
    </div> 
<!--end Modal cara pesan  -->

<!-- Modal sign-out -->
      <div class="modal fade" id="modLogout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #333; color: #fff">
              <button type="button" class="close" data-dismiss="modal" style="color: #fff"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user"></i><strong> Yakin Keluar?</strong></h4>
            </div>
            <div class="modal-footer">
              <a href="logout.php" class="btn btn-primary" style="background-color: #333; color: #fff"><i class="fa fa-fw fa-power-off"></i> Ya</a>
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Tidak</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end modal-->
</body>
</html>