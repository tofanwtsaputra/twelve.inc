<?php
    include 'koneksi.php';
    if (isset($_POST['search'])) {
      
        $qs = $_POST['search'];
?>
        <!DOCTYPE html>
        <html>
        <head>
            <title>Result Of <?php echo $qs; ?> | Twelve Inc </title>
        </head>
        <body>
            <section class="main-content">
                <div class="col-md-12 well">
                    <div class="span12">
                        <ul class="thumbnails listing-products" id="itemContainer">

                            <?php 
                                $query = "SELECT id_produk, kd_produk, nm_produk, desk_produk, image, harga_jual FROM produk WHERE nm_produk LIKE '%{$qs}%'";
                                            //print_r($query);die(); cek query error manual

                                $result = mysqli_query($conn, $query);
                                $no = mysqli_num_rows($result);
                                if ($no > 0) {
                                    # code...
                                
                                    //proses menampilkan data
                                    while($data = mysqli_fetch_array($result)) {

                            ?>
                                        <div class="alert alert-success" role="alert"><b><i class="fa fa-info-circle"></i> Menampilkan Hasil Pencarian Dari <?php echo $qs; ?></b></div>
                                        <div class="col-sm-3 col-sm-3 col-sm-3 col-sm-3 content">
                                            <div class="thumbnail">
                                                <a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>" title="<?php echo $data['nm_produk']; ?>"> <img src="image/produk/<?php echo  $data['image'];?>" alt="<?php echo  $data['image'];?>" width="250" height="375"></a> 
                                                <div class="caption">
                                                    <h4 class="pull-right"> Rp.<?php echo $dt=number_format($data['harga_jual']);?></h4>
                                                    <h4><a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>"> <?php echo custom_echo($data['nm_produk'],10);?></a>
                                                    </h4>
                                                </div>
                                                <a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>" class="btn btn-lg btn-block btn-success"><i class="fa fa-shopping-cart"></i> Beli Sekarang</a>
                                                <!-- <div class="ratings">
                                                    <p class="pull-right">18 reviews</p>
                                                    <p>
                                                        <span class="glyphicon glyphicon-star"></span>
                                                        <span class="glyphicon glyphicon-star"></span>
                                                        <span class="glyphicon glyphicon-star"></span>
                                                        <span class="glyphicon glyphicon-star"></span>
                                                        <span class="glyphicon glyphicon-star-empty"></span>
                                                    </p>
                                                </div> -->
                                            </div>
                                        </div>
                                     
                            <?php   }
                                }
                                else{
                                    echo '
                                    <div class="alert alert-danger" role="alert"><b><i class="fa fa-info-circle"></i> Hasil Untuk '.$qs.' Tidak Ditemukan !!!</b></div>
                                    ';
                                }

                                 ?>
                                
                        </ul>
                    </div>
                </div>

                        <div class="holder"></div>
            </section>
        </body>
        </html>
<?php  }?>  