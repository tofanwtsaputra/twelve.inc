<!DOCTYPE html>
<html>
<head>
  <title>About Us | Twelve Inc</title>
</head>
<body>

  <section class="main-content">
  <div class="col-md-12 well">
  <div class="col-md-12">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.994828976867!2d110.40720631406312!3d-7.790370994385865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a575d56935b97%3A0x2083067682590692!2sTWELVE+INC+(+Crows+Denim%2C+Bisnis+Distro)!5e0!3m2!1sid!2sid!4v1484224800217" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="">
      <p><i class="fa fa-info-circle"></i> Twelve Inc adalah perusahaan yang bergerak di bidang industri clothing / apparel pria.</p>
      <p><i class="fa fa-list"></i> Produk yang dipasarkan diantaranya jaket, blazer, t-shirt, kemeja, slayer, tas serta asesoris pria lainnya.</p>
      <p>
        Contact us:
      </p>
      <p><i class="fa fa-home"></i> TWELVE INC Jl. Raya Janti, Gang. Arjuna, No. 59, Karangjembe, Banguntapan, Bantul  55198. Telp : (0274) 4534571.
      </p>
      </div>
    </div>
  </section>
</body>
</html>