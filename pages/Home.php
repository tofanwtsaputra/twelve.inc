<!DOCTYPE html>
<html>
<head>
  <title>Home | Twelve Inc</title>
</head>
<body>

    <div id="slider" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <?php 
          $countbaner = "SELECT count(*) FROM banner";
          $rcb = mysqli_query($conn, $countbaner);
          $dtc = mysqli_fetch_array($rcb);
          $countb = $dtc['count(*)'];
         for ($i=0; $i < $countb; $i++) { 
            
            if ($i == 0) {
              $active = 'active';
            }
            else{
              $active = '';
            }
            echo'
                <li data-target="#slider" data-slide-to="'.$i.'" class="'.$active.'"></li>';
          }
         ?>
      </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <?php 
      $baner = "SELECT img_banner FROM banner";
        $rb = mysqli_query($conn, $baner);
        $i = 0;
        while ($dtb = mysqli_fetch_array($rb)) {
          if ($i == 0) {
            $active = 'active';
          }
          else{
            $active = '';
          }
         echo'        
              <div class="item '.$active.'">
                <img src="image/baner/'.$dtb["img_banner"].'" alt="'.$dtb["img_banner"].'">
              </div>';
          $i++;
        }  
       ?>
    </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <h3 align="center">Latest Product</h3><hr>
    <section class="main-content">
      <div class="col-md-12 well">
        <div class="span12">
          <ul class="thumbnails listing-products" id="itemContainer">

              <?php 
                  $query = "SELECT id_produk, kd_produk, nm_produk, desk_produk, image, harga_jual FROM produk Order By tgl_masuk desc limit 4";
                      //print_r($query);die(); cek query error manual

                  $result = mysqli_query($conn, $query);
                  $no = mysqli_num_rows($result);
                  //proses menampilkan data
                  while($data = mysqli_fetch_array($result)) {
              ?>
            <div class="col-sm-3 col-sm-3 col-sm-3 col-sm-3 content">
              <div class="thumbnail">
                <a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>" title="<?php echo $data['nm_produk']; ?>"> <img src="image/produk/<?php echo  $data['image'];?>" alt="<?php echo  $data['image'];?>" width="250" height="375"></a> 
                  <div class="caption">
                    <h5 class="pull-right"> Rp.<?php echo $dt=number_format($data['harga_jual']);?>
                    </h5>
                    <h5><a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>"> <?php echo custom_echo($data['nm_produk'],10);?></a>
                    </h5>
                  </div>
                  <a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>" class="btn btn-lg btn-block btn-success"><i class="fa fa-shopping-cart"></i> Beli Sekarang</a>
              </div>
            </div>
               
        <?php } ?>
          </ul>
        </div><!-- 
        <div align="center">
           <a class="btn btn-primary" href="index.php?p=Order_now"><i class="fa fa-shopping-cart"> Order Now</i></a>
        </div> -->
      </div>
    </section>
  
</body>
</html>