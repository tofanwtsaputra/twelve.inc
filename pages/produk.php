<?php
	include 'koneksi.php';
	$orderid = $_REQUEST['id'];
	$view_order = "SELECT id_produk,kd_produk, nm_produk, desk_produk, image, balance,
			   		harga_jual ,berat ,produk.id_sablon, nm_sablon, produk.id_bahan, nm_bahan FROM produk 
			   				INNER JOIN sablon ON (produk.id_sablon = sablon.id_sablon)
			   				INNER JOIN bahan ON ( produk.id_bahan = bahan.id_bahan)
			   				WHERE id_produk = {$orderid}"; 
			   				//print_r($view_order);die();
	$query_view_order = mysqli_query($conn, $view_order);
	$view = mysqli_fetch_array($query_view_order);
	$no = $view['kd_produk'] ;
	$name = $view['nm_produk'];
	$desc = $view['desk_produk'];
	$price = number_format($view['harga_jual'], 2) ;
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>View Product | Twelve Inc</title>
</head>
<body>
	<form action="add_order_produk.php" method="POST">
			<div class="col-md-12 well">
			 	<div class="col-md-12">
			 		<h1 itemprop="name" align="left"><?php echo $view['nm_produk']; ?></h1><br>
			 	</div> 
			 	<div class="row">
			    	<div class="col-md-4"> 
					<!-- show image-->
			            <p align="center">
			            	<a class="group1 cboxElement" href="image/produk/<?php echo $view['image']; ?>" title="<?php echo $view['nm_produk']; ?>"><img itemprop="image" src="image/produk/<?php echo $view['image']; ?>" alt="">
			            	</a>
			            </p>	  		  
			        </div>
			                        	
			        <div class="col-md-4">
						<div class="row stock-share">
							<!-- <div class=""> -->
			                    <h3 itemprop="Detail">Detail Produk :</h3>
								<dl class="dl-horizontal">
				                        <!--dt>Brand:</dt><dd><a href="#">Crows Denim</a></dd-->
				                        <dt itemprop="price">Harga :</dt>
											<dd><p>Rp <?php echo $dt=number_format($view['harga_jual'],2);?></p></dd>
										
				                        <dt>Product Code :</dt>
				                        	<dd><p><?php echo $view['kd_produk']; ?></p></dd>
				                        <dt itemprop="availability">Availability :</dt>
				                        	<dd><p><?php echo $view['balance']; ?></p></dd><!--stok properti-->
			                            <dt>Material :</dt>
											<dd><p><?php echo $view['nm_bahan']; ?></p></dd>
			                            <dt>Sablon :</dt>
											<dd><p><?php echo $view['nm_sablon']; ?></p></dd>
			                            <dt>Berat :</dt>
											<dd><?php echo $view['berat'].' Kg'; ?></dd>
			                  	</dl>
			                <!-- </div> -->
			            	
				        </div><!--row-stock-share-->

			        <div>						
						            <div class="form-horizontal">		            	
							            <div class="form-group required">	
							            	<label class="control-label col-sm-2" for="input-option253">Size</label>
							            		<div class="col-sm-10">
							            			<select name="ukuran" id="input-option253" class="form-control"><!-- 
								                        <option value="0"></option> -->
								                        <option value="1">S (Small)</option> <!--173-->
								                        <option value="2">M (Medium)</option>
								                        <option value="3">L (Large)</option>
								                        <option value="4">XL (Ekstra Large)</option>
								                        <option value="5">XXL (Double Ekstra Large)(+IDR 15,000)</option>
								                        <option value="6">XXXL (Triple Extra Large)(+IDR 20,000)</option>
							                        </select>	  
												</div>
						          		</div>
						          		<div class="form-group required">
							              <label class="control-label col-sm-2 " for="input-quantity">Qty</label>
								              <div class="col-sm-10">
								              	<input name="jumlah" value="1" size="2" id="input-quantity" class="form-control" type="text"
								              	onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
								              	onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')">
								              </div>
							              <input name="product_id" value="<?php echo $view['id_produk']; ?>" type="hidden">
							              <br>
							            </div>
										<div class="form-group">			
							            	<div class="col-sm-10 col-sm-offset-2">
								            	<?php 
									            	if (isset($_SESSION['customer'])) {
									            		echo ('<button type="submit" class="btn btn-success btn-lg btn-block pull-right">
															<i class="fa fa-shopping-cart"></i> Beli Sekarang
														</button>');
									            	}
									            	else{
														echo("<a class='btn btn-success btn-lg btn-block pull-right' data-toggle='modal' data-target='#modalRegister' href='#'><i class='fa fa-shopping-cart'></i> Beli Sekarang</a>");
														//echo($_SESSION['customer']);
									            	}

									             ?>
											</div>
										</div>
									</div><!-- form-horizontal -->
							</div><!--product-->


			        </div><!--col-md-7-->
			        <div class="col-md-4">
			        		  
					   		<p itemprop="description"><h3>Deskripsi :</h3></p>
						   		<dl class="dl-horizontal">				   					
									<blockquote><p><?php echo $view['desk_produk'];?></p></blockquote>
										
								</dl>		  
			        </div>
			    </div><!-- row -->	
			</div>

			<h4><center><strong>Random Product</strong></center></h4>
						<?php
							$r=rand(1, 3);
							$query = "SELECT id_produk, kd_produk, nm_produk, desk_produk, image, harga_jual FROM produk WHERE kd_kategori=$r limit 6 ";
							//print_r($query);die(); //cek query error manual

							$result = mysqli_query($conn, $query);
							$no = 0;
							//proses menampilkan data
							while($data = mysqli_fetch_array($result)) {
						?>

		            <div class="col-sm-2 content">
		              <div class="thumbnail">
		                <a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>" title="<?php echo $data['nm_produk']; ?>"> <img src="image/produk/<?php echo  $data['image'];?>" alt="<?php echo  $data['image'];?>" width="150" height="275"></a> 
		                  <div class="caption">
		                    <h5 class="pull-right"> Rp.<?php echo $dt=number_format($data['harga_jual']);?>
		                    </h5>
		                    <h5><a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>"> <?php echo custom_echo($data['nm_produk'],10);?></a>
		                    </h5>
		                  </div>
		                  <!-- <div class="ratings">
		                    <p class="pull-right">18 reviews</p>
		                    <p>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star-empty"></span>
		                    </p>
		                  </div> -->
		              </div>
		            </div>
						<?php } ?>  
	</form>
</body>
</html>