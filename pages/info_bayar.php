<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
  <title>Finish Order | Twelve Inc</title>
</head>
<body>


<?php    
//echo $_SESSION['nota'];
if (empty($_SESSION['nota'])) {
  echo'<br>
          <div class="alert alert-danger" role="alert"><center><h1><i class="fa fa-info-circle"></i> 
            Daftar <a href="/index.php" class="alert-link">Belanja </a>Anda Masih Kosong</center></h1>
          </div><br>';
          //header('location:index.php');
          
 } 

 else {
    $id_customer=$_SESSION['customer'];

  /*--query untuk mengambil info order--*/
      $nota = $_SESSION['nota'];
      $q_info_order = "SELECT * FROM orders 
                   WHERE no_nota='$nota'";

          $hasil_nota = mysqli_query($conn,$q_info_order) or die($q_info_order);
          $data_order = mysqli_fetch_array($hasil_nota);
              
      
      /*--query untuk menampilkan inform
                              <li><a href="crpesan.php"><i class="fa fa-question-circle"></i> Cara Pesan</a></li>asi customer--*/
          $q_info_customer = "SELECT * FROM customer, kota 
                   WHERE customer.id_kota=kota.id_kota 
                   AND id_cust=$id_customer";

          $hasil = mysqli_query($conn,$q_info_customer) or die($q_info_customer);
          $data_cust = mysqli_fetch_array($hasil);

          /*--query untuk menampilkan informasi order detail*/
          
          $q_order_detail = "SELECT * FROM order_detail
                   WHERE no_nota = $nota";
          $hsil = mysqli_query($conn,$q_order_detail) or die($q_order_detail);


                /*mendapatkan biaya pengiriman*/
                $kt= $data_cust['nm_kota'];
                $q_kota = "SELECT ongkos FROM kota WHERE nm_kota='$kt'";
                $r=mysqli_query($conn, $q_kota);
                $d_kota = mysqli_fetch_array($r);

          /*--query untuk menampilkan info rekening pembayaran */
          $qbank = "SELECT nm_bank, no_rekening, pemilik FROM bank";
          $rbank = mysqli_query($conn, $qbank);
          $dtbank = mysqli_fetch_array($rbank);

      ?>
      <div class="container-fluid">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><strong><i class="fa fa-info-circle"></i> Proses Transaksi Selesai</strong></h4>
        </div>

        <!-- <div class="col-md-6"> -->
          <div class="panel panel-default">
            <div class="panel-heading">
                <center><h4><b><i class="fa fa-user"></i> Data Pemesan beserta <i class="fa fa-shopping-cart"></i> Data Order adalah Sebagai Berikut</b></h4></center>
            </div>
            <div class="panel-body">
                <div class="alert alert-default">
                   <table class="table table-responsive">
                    <tr>
                      <td><strong><i class="fa fa-user"></i> Nama </strong></td><td>  <b><?php echo $data_cust['nm_lengkap']; ?></b> </hr></td>
                    </tr>
                    <tr>
                      <td><strong><i class="fa fa-home"></i> Alamat Lengkap  </strong></td><td>  <b><?php echo $data_cust['alamat']; ?> <?php echo  $data_cust['nm_kota']; ?>  <?php echo $data_cust['kode_pos']; ?></b></td>
                    </tr>
                    <tr>
                      <td><strong><i class="fa fa-phone"></i> Telpon          </strong></td><td>  <b><?php echo $data_cust['telepon']; ?> </b></td>
                    </tr>
                    <tr>
                      <td><strong><i class="fa fa-envelope"></i> E-mail          </strong></td><td>  <b><?php echo $data_cust['email']; ?> </b></td>
                    </tr>
                   </table>
                </div><hr>

                   <div class="alert alert-danger" role="alert"><b>Nomor Nota : <?php echo $nota;?>  <span class="pull-right"><i class="fa fa-info-circle"></i> Harap Catat Informasi Ini Untuk Melakukan Konfirmasi Pembayaran Atau Complain</span></b></div>
                
               <?php 
                /*jika data order detai ditemukan*/
                $tmpberat = 0;
                while ($data_ordtl=mysqli_fetch_array($hsil)) {


                  /* mendapatkan kode, nama produk*/
                  $id_produk=$data_ordtl['id_produk'];
                  $q_produk = "SELECT * FROM produk WHERE id_produk=$id_produk";
                  $data_p=mysqli_fetch_array(mysqli_query($conn, $q_produk));
                  //print_r($q_produk);//die();

                  /* mendapatkan kode, nama produk*/
                  $id_ukuran=$data_ordtl['id_ukuran'];
                  $q_ukuran = "SELECT harga_ukuran, nm_ukuran  FROM ukuran WHERE id_ukuran=$id_ukuran";
                  $data_u=mysqli_fetch_array(mysqli_query($conn,$q_ukuran));
                  //print_r($q_ukuran);die();

                  echo "<div class='alert alert-default' role='alert'><strong><i class='fa fa-shopping-cart'></i> ".$data_p['kd_produk']." -- ".$data_p['nm_produk']." | Size : ".$data_u['nm_ukuran']." (+Rp.".number_format($data_u['harga_ukuran'],2).") | Qty : ".$data_ordtl['qty_order']." | Discount : ".number_format($data_ordtl['disc'],2)."% | Berat : ".$data_p['berat']." Kg<span class='pull-right'> Rp. ".number_format($data_ordtl['sub_total'],2)."</span></strong>
                   </div>";
                   $tmpberat+=$data_p['berat'];
                }
                $berat = ceil($tmpberat);
                  echo"<hr>
                        <div class='alert alert-default' role='alert'>
                          <strong><i class='fa fa-plane'></i> Biaya Pengiriman Ke ".$data_cust['nm_kota']." Rp.".number_format($data_cust['ongkos'])." /kg<span class='pull-right'>Rp. ".number_format($data_cust['ongkos']*$berat,2)."</span></strong>
                        </div>

                        <div class='alert alert-default' role='alert'>
                          <strong><i class='fa fa-dollar'></i>   Grand Total <span class='pull-right'>Rp. ".number_format($data_order['grand_total'],2)."</span></strong>
                        </div>";
                ?>
            </div>
            <div class="panel-footer">
              <h4>
                <b>
                  <?php
                    echo' Silahkan lakukan pembayaran sebanyak <code>Grand Total</code> yang tercantum ke Nomor Rekening: <code>'.$dtbank["nm_bank"].' - '.$dtbank["no_rekening"].'</code><br> Atas Nama: <code>'.$dtbank["pemilik"].'</code><hr />
                      <p> 
                       Apabila Anda tidak melakukan <code>Pembayaran</code> dan <code>Konfirmasi</code> dalam <code>2 x 24 jam</code>, maka transaksi dianggap <code>Batal</code>. Terimakasih
                      </p>';
                      ?>
                </b>
              </h4>        
            </div>
          </div>
        <!-- </div> -->
       </div><!-- container -->
 <?php
      //}//close jml
  } //close empty session['customer']
  ?>

</body>
</html>