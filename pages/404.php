<!DOCTYPE html>
<html>
<head>
  <title>Error 404 Page Not Found</title>
</head>
<body>
  <!-- Jumbotron -->
  <div class="jumbotron">
    <h1><center><i class="fa fa-frown-o red"></i> Aw..Snap!!</center></h1>
    <p align="center" class="lead">Page that you're looking for could not be found  :( <em><span id="display-domain"></span></em>.</p>
    <!-- <p align="center"><a class="btn btn-default btn-lg"><span class="green">Take Me To The Homepage</span></a> -->
       <!--  <script type="text/javascript">
            function checkSite(){
              var currentSite = window.location.hostname;
                window.location = "http://" + currentSite;
            }
        </script> -->
    <!-- </p> -->
  </div>
  <div class="body-content">
    <div class="row">
      <div class="col-md-6">
        <h2>What happened?</h2>
        <p class="lead">A 404 error status implies that the file or page that you're looking for could not be found.</p>
      </div>
      <div class="col-md-6">
        <h2>What can I do?</h2>
        <p class="lead"><a class="btn btn-primary" href="index.php?p=Home">Back to Home</a></p>
     </div>
    </div>
  </div>


</body>
</html>