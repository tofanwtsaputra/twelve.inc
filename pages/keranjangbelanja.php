		<!DOCTYPE html>
		<html>
		<head>
			<title>Shopping Cart | Twelve Inc</title>
		</head>
		<body>

<?php 
	if (empty($_SESSION['customer'])) {
?>		
		<div class="col-md-12 well">
			<div class="row" >
				<div class="thumbnail" align="left">
		 			<h4 align="center">
		 				<strong><i class="fa fa-shopping-cart"></i> Daftar Belanja Anda</strong>
		 			</h4><hr>
		 				<div class='alert alert-danger' role='alert'><b><i class='fa fa-info-circle'></i> Daftar Belanja Masih Kosong</b></div>
		 		</div>
		 		<div>
					<a class='btn btn-primary btn-large' href='javascript:history.go(-2)'><i class="fa fa-shopping-cart"></i> Belanja Produk Lagi</a>
					<span class="pull-right">
				</div>
		 	</div>
		</div>
<?php

	}
	else{
			session_id();
			include'koneksi.php';
			$session = session_id();
			$query_view_order = "SELECT produk.id_produk, kd_produk, nm_produk, harga_jual, disc, id_order_temp,((harga_jual*disc+harga_jual+harga_ukuran)*qty_order) as total, session_order, qty_order, nm_ukuran, harga_ukuran, berat from produk inner join order_tmp on (produk.id_produk = order_tmp.id_produk) inner join ukuran on (order_tmp.id_ukuran = ukuran.id_ukuran) 
				WHERE session_order = '$session'" ;
			//print_r($query_view_order);die();
			$result = mysqli_query($conn, $query_view_order);
			$row = mysqli_num_rows($result);
		 ?>
		<div class="col-md-12 well">
			<div class="row" >
				<div class="thumbnail" align="left">

		 			<h4 align="center">
		 				<strong><i class="fa fa-shopping-cart"></i> Daftar Belanja Anda</strong>
		 			</h4><hr>
		 					<?php 
		 						if ($row > 0) {
			 						$gtotal=0;
						 			while ($data = mysqli_fetch_array($result)) {
								 			extract($data);
								 			//$disc = 10/100;
								 			$gtotal += $total;
					 		 ?>
		 						<dl>
		 							<span>
		 							<strong>
		 							<a href="hapus_order_tmp.php?id=<?php echo $id_order_temp;?>"><button class="btn btn-danger"><i class="fa fa-trash-o"></i></button></a> 
		 								<?php echo $kd_produk;?> -- <?php echo $nm_produk;?> (Qty : <?php echo $qty_order;?>  | Disc : <?php echo $disc;?> |Size : <?php echo $nm_ukuran; ?> |Berat : <?php echo $berat; ?> Kg) <span class="pull-right"> Rp. <?php echo number_format($total,2);?> </span>
		 							</strong>
		 							</span>
		 						</dl>	
		 					<?php
					 				}//end while
					 		?>
					 			<dl>
					 				<hr>
					 				<strong>Total<span class="pull-right">Rp. <?php echo number_format($gtotal,2);?></span></strong>
					 			</dl>
					 		<?php	
					 			}
					 			else {

					 			echo("<div class='alert alert-danger' role='alert'><b><i class='fa fa-info-circle'></i> Daftar Belanja Masih Kosong</b></div>");
					 			}
					 		?>
		 		</div>

		 		<div>
					<a class='btn btn-primary btn-large' href='javascript:history.go(-2)'><i class="fa fa-shopping-cart"></i> Belanja Produk Lagi</a>
					<span class="pull-right">
					<?php if ($row>0) {
						echo("<a class='btn btn-success' data-toggle='modal' data-target='#modalKirim' href='#'><i class='fa fa-share'></i> Check-Out</a>");
					} ?>
				</div>
		 	</div>
		</div>
		<!-- Modal kirim-->
			<?php 
				$qcust = "SELECT * FROM customer WHERE id_cust = {$_SESSION['customer']}";
				$rcust = mysqli_query($conn, $qcust);
				$dcust = mysqli_fetch_array($rcust);
				extract($dcust);
			 ?>
		    <div class="modal fade in" id="modalKirim" role="dialog" aria-hidden="true">

		        <div class="modal-dialog modal-md">
		          <!-- Modal content-->
		          <div class="modal-content">
		            <div class="modal-header" style="background-color: #333; color: #fff">
		              <button type="button" class="close" data-dismiss="modal" style="color: #fff">&times;</button>
		              <h2><center><span class="fa fa-plane"></span> Pengiriman Produk</center></h2>
		            </div>
		            <div class="modal-body">
		                <form role="form" name="kirim" action="kirim_proccess.php" method="post">

		                    <div class="form-group">
		                        <label><span><i class="fa fa-plane"></i> Kirim Ke: </label>
		                         <input type="hidden" name="id_cust" value="<?php echo $_SESSION['customer'];?>">
		                    <div class="form-group">
		                        <label><span><i class="fa fa-globe"></i> Kota Tujuan</span></label>
		                        <select name="kota" class="form-control" value="<?php echo $id_kota; ?>" id="kota">
		                            <?php 
		                                $qkota = "SELECT * FROM kota";
		                                $result = mysqli_query($conn, $qkota);
		                                while ($data = mysqli_fetch_array($result)) {
		                                    echo('
		                                        <option value="'.$data["id_kota"].'" ');
		                                    echo $k = ($id_kota==$data["id_kota"]) ? 'selected' : '';
		                                    echo('> '.$data["nm_kota"].'</option>');
		                                }
		                              ?>
		                        </select>
		                    </div>
		                    </div>   
		                    <div class="form-group">
		                        <label><span><i class="fa fa-home"></i> Alamat Lengkap</span></label>
		                        <textarea class="form-control" name="alamat" id="alamat" required placeholder="Alamat Lengkap.. "><?php echo $alamat; ?></textarea>
		                    </div>     
		                    <div class="form-group">
		                        <label><span><i class="fa fa-envelope"></i> Kode Pos</span></label>
		                        <input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Kode Pos"
		                                 onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
		                                 onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
		                                  required value="<?php echo $kode_pos; ?>">
		                    </div>
		                    <div class="form-group">
		                        <label><span><i class="fa fa-phone"></i> No Telepon yang dapat dihubungi</span></label>
		                        <input type="text" class="form-control" name="telepon" id="telepon" placeholder="No Telepon"
		                                 onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
		                                 onkeyup="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
		                                  required value="<?php echo $telepon; ?>">
		                    </div>    
		                    <div class="form-group">
		                        <button type="submit" class="btn btn-success btn-block" name="kirim" style="background-color: #333; color: #fff"><span class="glyphicon glyphicon-off"></span> Kirim</button>
		                    </div>  
		                </form>
		            </div>
		          </div>
		          
		        </div>
		    </div> 
		<!--end Modal Kirim  -->

		<br>		
		<div>		
			<h4><center><strong>Random Product</strong></center></h4>
						<?php
							$r=rand(1, 3);
							$query = "SELECT id_produk, kd_produk, nm_produk, desk_produk, image, harga_jual FROM produk WHERE kd_kategori=$r limit 6 ";
							//print_r($query);die(); //cek query error manual

							$result = mysqli_query($conn, $query);
							$no = 0;
							//proses menampilkan data
							while($data = mysqli_fetch_array($result)) {
						?>

		            <div class="col-sm-2 content">
		              <div class="thumbnail">
		                <a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>" title="<?php echo $data['nm_produk']; ?>"> <img src="image/produk/<?php echo  $data['image'];?>" alt="<?php echo  $data['image'];?>" width="150" height="275"></a> 
		                  <div class="caption">
		                    <h5 class="pull-right"> Rp.<?php echo $dt=number_format($data['harga_jual']);?>
		                    </h5>
		                    <h5><a href="index.php?p=produk&id=<?php echo $data['id_produk'];?>"> <?php echo custom_echo($data['nm_produk'],10);?></a>
		                    </h5>
		                  </div>
		                  <!-- <div class="ratings">
		                    <p class="pull-right">18 reviews</p>
		                    <p>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star"></span>
		                      <span class="glyphicon glyphicon-star-empty"></span>
		                    </p>
		                  </div> -->
		              </div>
		            </div>
						<?php } ?>
		</div>	
<?php 
		}
 ?>
		</body>
		</html>
	