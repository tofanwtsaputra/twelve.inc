# README.md #

This is a sample e-commerce website.

* Version 1.1.0

### Contributor ###

* Tofan Wahyu Tri Saputra

## Instalation ##

1. Import <code>Database</code> from directory /twelve2/db.

2. Setting connection file <code>koneksi.php</code> from root folder, change value of host , username , password  and database name as mysql setting from your computer.

3. Default accsess /twelve2/backend/ (administrator page's):
	
	email : mastopp@gmail.com

	password : admin

4.	Add manual admin account with DBMS like PHP My Admin, or something else you like to admin's table in database, If u want to add new user for access administrator site's. And don't forget to hash the password while adding it with <code>sha1</code>.

5. Example Query for adding new admin.

INSERT INTO admin (email, password) VALUES('something@gmail.com', sha1('your_password'));

6. Enjoy.