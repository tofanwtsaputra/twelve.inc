-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: twelve
-- ------------------------------------------------------
-- Server version	5.5.53-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id_usr` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` char(40) NOT NULL,
  `level` tinyint(4) DEFAULT '3',
  PRIMARY KEY (`id_usr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'mastopp','mastopp@gmail.com','d033e22ae348aeb5660fc2140aec35850c4da997',1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bahan`
--

DROP TABLE IF EXISTS `bahan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bahan` (
  `id_bahan` int(11) NOT NULL AUTO_INCREMENT,
  `nm_bahan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_bahan`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bahan`
--

LOCK TABLES `bahan` WRITE;
/*!40000 ALTER TABLE `bahan` DISABLE KEYS */;
INSERT INTO `bahan` VALUES (1,'Drill Track'),(2,'Cotton'),(3,'Fleece'),(4,'Poly  Adidas'),(5,'Soft Cotton Fleece'),(6,'Drill Import'),(7,'Nike Fleece'),(8,'Cotton Canvas'),(9,'Cotton Combed'),(10,'Baby Terry'),(11,'Taipan Drill'),(12,'Cadilac');
/*!40000 ALTER TABLE `bahan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `id_bank` int(11) NOT NULL AUTO_INCREMENT,
  `nm_bank` varchar(50) NOT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,'BRI','595301001234567','Tofan Wahyu Tri Saputra');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `nm_banner` varchar(50) DEFAULT NULL,
  `img_banner` varchar(50) NOT NULL,
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'e10','baner1.png'),(2,'e11','baner2.png'),(3,'e08','baner3.png'),(4,'e06','baner4.png');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id_cust` int(11) NOT NULL AUTO_INCREMENT,
  `nm_lengkap` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `pass` char(40) NOT NULL,
  PRIMARY KEY (`id_cust`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'nurrohman','jl. gajah no.9 rt.04 rw.01 warung boto yogyakarta',55164,'nur@gmail.com','83867084605',1,'12dea96fec20593566ab75692c9949596833adc9'),(5,'Lukman','jogja',53453,'lukman@gmail.com','899456234',1,'33d1dc7c53f3ea2e08bc6a5b9f50c7e972ff5614'),(6,'Sony','jogja',56612,'sony@gmail.com','098976753',1,''),(7,'Sony','jogja',56612,'sony@gmail.com','098976753',1,''),(8,'tofan','solo',59174,'tofanwts@gmail.com','8384032',2,''),(9,'yoshi','jl.Gajah, no.9 Glagahsari, Umbulharjo. Yogyakarta',59174,'yoshi@gmail.com','08995756091',1,'27468a68a913c8cd41521edca6b614de1d16b121');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `kd_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kategori` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`kd_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'Blazer'),(2,'Jaket'),(3,'Kaos'),(4,'Aksesoris');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `konfirmasi`
--

DROP TABLE IF EXISTS `konfirmasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konfirmasi` (
  `id_konfirm` int(11) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(30) NOT NULL,
  `nm_bank` varchar(30) DEFAULT NULL,
  `no_rek` varchar(30) DEFAULT NULL,
  `a_nama` varchar(50) DEFAULT NULL,
  `tujuan` varchar(30) DEFAULT NULL,
  `bukti` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_konfirm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `konfirmasi`
--

LOCK TABLES `konfirmasi` WRITE;
/*!40000 ALTER TABLE `konfirmasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `konfirmasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kota`
--

DROP TABLE IF EXISTS `kota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kota` (
  `id_kota` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kota` varchar(30) NOT NULL,
  `ongkos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_kota`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kota`
--

LOCK TABLES `kota` WRITE;
/*!40000 ALTER TABLE `kota` DISABLE KEYS */;
INSERT INTO `kota` VALUES (1,'Yogyakarta',5000),(2,'Solo',0),(3,'Magelang',0),(4,'Semarang',0),(5,'Jakarta',0),(6,'Salatiga',0);
/*!40000 ALTER TABLE `kota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail` (
  `id_order_det` int(11) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(50) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_ukuran` int(11) DEFAULT '0',
  `qty_order` tinyint(4) NOT NULL,
  `sub_total` int(11) NOT NULL,
  `disc` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_order_det`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` VALUES (1,'1482072394',15,1,1,235000,0),(2,'1482072394',3,3,1,200000,0),(3,'1482424544',22,1,1,229000,0),(4,'1483629206',1,1,3,600000,0),(5,'1483629206',24,1,1,235000,0);
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tmp`
--

DROP TABLE IF EXISTS `order_tmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tmp` (
  `id_order_temp` int(11) NOT NULL AUTO_INCREMENT,
  `session_order` varchar(30) NOT NULL,
  `qty_order` tinyint(3) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_ukuran` int(11) NOT NULL,
  `tgl_order` date DEFAULT NULL,
  PRIMARY KEY (`id_order_temp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tmp`
--

LOCK TABLES `order_tmp` WRITE;
/*!40000 ALTER TABLE `order_tmp` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_tmp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `no_nota` varchar(50) NOT NULL,
  `tgl_order` date NOT NULL,
  `id_cust` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `grand_total` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`no_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('1482072394','2016-12-18',9,2,450000,'lunas'),('1482424544','2016-12-22',9,1,234000,'lunas'),('1483629206','2017-01-05',9,4,845000,'belum');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL AUTO_INCREMENT,
  `kd_produk` varchar(5) NOT NULL,
  `nm_produk` varchar(50) NOT NULL,
  `desk_produk` varchar(200) DEFAULT NULL,
  `image` varchar(50) NOT NULL,
  `id_sablon` int(11) DEFAULT NULL,
  `id_bahan` int(11) DEFAULT NULL,
  `harga_jual` decimal(10,0) NOT NULL,
  `kd_kategori` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `balance` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL DEFAULT '0',
  `disc` int(11) NOT NULL,
  `berat` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produk`
--

LOCK TABLES `produk` WRITE;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` VALUES (1,'sk82','Blazer Blue Crows Denim','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','sk82.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(2,'sk83','Blazer Blue Crows Denim 4','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','sk83.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(3,'sk84','Blazer Blue Crows Denim 3','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','sk84.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(4,'bk01','Blazer Single Button','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','bk01.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(5,'ks15','Blazer Korean Style','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','ks15.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(6,'ks29','Blazer Korean Lee Min Hoo','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','ks29.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(7,'ks30','Blazer Korean Style Abu-abu Lengan Pendek','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','ks30.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(8,'ks33','Blazer Korean Style Black','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','ks33.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(9,'ks59','Blazer Korean Style Pria Abu-Abu','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','ks59.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(11,'sk65','Blazer Korean Style Cool ','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','sk65.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(12,'sk103','Fit Blazer ','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','sk103.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(13,'sk46','Blazer Aliando GGS ','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','sk46.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50),(14,'b9','T.F.O.A Bandana','-','b9.png',1,9,65000,4,'2016-04-21',10,45500,0,2.00),(15,'tr03','Mancester United Backpack','-','tr03.png',1,1,235000,4,'2016-04-21',10,164500,0,2.00),(16,'h6','Harumichi Chibi','-','h6.png',1,1,110000,3,'2016-04-21',10,77000,0,0.23),(17,'h3','T-Shirt - TFOA','-','h3.png',1,1,110000,3,'2016-04-21',10,77000,0,0.23),(18,'h4','T-Shirt - TFOA 2','-','h4.png',1,1,110000,3,'2016-04-21',10,77000,0,0.23),(19,'g3','Genji Neck Skull','-','g3.png',1,1,110000,3,'2016-04-21',10,77000,0,0.23),(20,'n12','Black Green Hoodie - Shikamaru Nara','-','n12.png',1,3,225000,2,'2016-04-21',10,157500,0,0.70),(21,'z04','Green Hoodie','-','z04.png',1,5,230000,2,'2016-04-21',10,161000,0,0.70),(22,'tg6','Black Hard Hoodie - Tokyo Ghoul','-','tg6.png',1,10,229000,2,'2016-04-21',10,160300,0,0.70),(23,'s7','Gakuran School Premium Edition','-','s7.png',1,6,225000,2,'2016-04-21',10,157500,0,0.70),(24,'n13','Gaara - Naruto Shippuden','-','n13.png',1,5,235000,2,'2016-04-21',10,164500,0,0.70),(25,'n10','Dewa Kematian - Shiki Fuujin','-','n10.png',1,5,235000,2,'2016-04-21',10,164500,0,0.70),(26,'ks8','Jacket Korean Double Zipper','-','ks8.png',1,1,270000,2,'2016-04-21',10,189000,0,0.70),(27,'gps','Genji Perfect Seiha','-','gps.png',1,1,225000,2,'2016-04-21',10,157500,0,0.70),(28,'gs1','Genji Sporty Edition','-','gs1.png',1,4,225000,2,'2016-04-21',10,157500,0,0.70),(29,'e12','Blackbeard Pirates ','-','e12.png',1,1,230000,2,'2016-04-21',10,161000,0,0.70),(30,'e6','Monkey D. Luffy','Jaket ini sangat cocok buat kalian para pecinta Anime One Piece','e6.png',1,5,235000,2,'2016-04-21',10,164500,0,0.70),(31,'e9','Roronoa Zoro','-','e9.png',1,10,235000,2,'2016-04-21',10,164500,0,0.70),(32,'e7','Marine','-','e7.png',1,5,225000,2,'2016-04-21',10,157500,0,0.70),(33,'z03','Shingeki no Kyojin - Scouting Legion','J','z03.png',1,8,225000,2,'2016-04-21',10,157500,0,0.70),(34,'gg1','Gakuran Suzuran','-','gg1.png',1,11,205000,2,'2016-04-21',10,143500,0,0.70),(35,'gh1','Gakuran Housen Gakuen','-','gh1.png',1,12,205000,2,'2016-04-21',10,143500,0,0.70),(36,'bk02','Blazer Korean Style Black 2','Blazer ini selain harganya terjangkau, juga terbuat dari bahan yang berkualitas sehingga dapat membuatmu tampil maksimal ketika dikenakan.','bk02.png',1,1,200000,1,'2016-04-21',10,140000,0,0.50);
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retur`
--

DROP TABLE IF EXISTS `retur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retur` (
  `id_retur` int(11) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(30) NOT NULL,
  `jml_retur` int(11) DEFAULT NULL,
  `tgl_retur` date NOT NULL,
  `ket_retur` varchar(50) NOT NULL,
  `id_cust` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_retur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retur`
--

LOCK TABLES `retur` WRITE;
/*!40000 ALTER TABLE `retur` DISABLE KEYS */;
/*!40000 ALTER TABLE `retur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retur_detail`
--

DROP TABLE IF EXISTS `retur_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retur_detail` (
  `id_retur_det` int(11) NOT NULL AUTO_INCREMENT,
  `no_nota` varchar(30) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_ukuran` int(11) DEFAULT NULL,
  `qty_retur` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_retur_det`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retur_detail`
--

LOCK TABLES `retur_detail` WRITE;
/*!40000 ALTER TABLE `retur_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `retur_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sablon`
--

DROP TABLE IF EXISTS `sablon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sablon` (
  `id_sablon` int(11) NOT NULL AUTO_INCREMENT,
  `nm_sablon` varchar(50) NOT NULL,
  PRIMARY KEY (`id_sablon`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sablon`
--

LOCK TABLES `sablon` WRITE;
/*!40000 ALTER TABLE `sablon` DISABLE KEYS */;
INSERT INTO `sablon` VALUES (1,'Rubber');
/*!40000 ALTER TABLE `sablon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stok`
--

DROP TABLE IF EXISTS `stok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stok` (
  `id_stok` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) NOT NULL,
  `jml_masuk` int(11) DEFAULT NULL,
  `jml_keluar` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `stok_real` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_stok`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stok`
--

LOCK TABLES `stok` WRITE;
/*!40000 ALTER TABLE `stok` DISABLE KEYS */;
/*!40000 ALTER TABLE `stok` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ukuran`
--

DROP TABLE IF EXISTS `ukuran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ukuran` (
  `id_ukuran` int(11) NOT NULL AUTO_INCREMENT,
  `nm_ukuran` varchar(4) NOT NULL,
  `harga_ukuran` int(11) NOT NULL,
  PRIMARY KEY (`id_ukuran`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ukuran`
--

LOCK TABLES `ukuran` WRITE;
/*!40000 ALTER TABLE `ukuran` DISABLE KEYS */;
INSERT INTO `ukuran` VALUES (1,'S',0),(2,'M',0),(3,'L',0),(4,'XL',0),(5,'XXL',15000),(6,'XXXL',20000);
/*!40000 ALTER TABLE `ukuran` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-10 10:38:08
