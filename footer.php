 <!-- footer -->  
  <footer id="aa-footer">
    <!-- footer bottom -->
    <div class="aa-footer-top">
     <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-top-area">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <h3>Main Menu</h3>
                  <ul class="aa-footer-nav">
                    <li><a href="index.php">Home</a></li><!-- 
                    <li><a href="#">Our Services</a></li>
                    <li><a href="#">Our Products</a></li> -->
                    <li><a href="index.php?p=about">About Us</a></li><!-- 
                    <li><a href="#">Contact Us</a></li> -->
                  </ul>
                </div>
              </div><!-- 
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>Knowledge Base</h3>
                    <ul class="aa-footer-nav">
                      <li><a href="#">Delivery</a></li>
                      <li><a href="#">Returns</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Discount</a></li>
                      <li><a href="#">Special Offer</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>Useful Links</h3>
                    <ul class="aa-footer-nav">
                      <li><a href="#">Site Map</a></li>
                      <li><a href="#">Search</a></li>
                      <li><a href="#">Advanced Search</a></li>
                      <li><a href="#">Suppliers</a></li>
                      <li><a href="#">FAQ</a></li>
                    </ul>
                  </div>
                </div>
              </div> -->
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>Contact Us</h3>
                    <address>
                      <p> Jl. Raya Janti, Gg. Arjuna No.59 Karangjambe, Banguntapan, Bantul - Yogyakarta</p>
                      <p><span class="fa fa-phone"></span>+62 8384-0318-097</p>
                      <p><span class="fa fa-envelope"></span>tofanwts@gmail.com</p>
                    </address>
                    <!-- <div class="aa-footer-social">
                      <a href="#"><span class="fa fa-facebook"></span></a>
                      <a href="#"><span class="fa fa-twitter"></span></a>
                      <a href="#"><span class="fa fa-google-plus"></span></a>
                      <a href="#"><span class="fa fa-youtube"></span></a>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>
    </div>
    <!-- footer-bottom -->
    <div class="aa-footer-bottom">
      <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-bottom-area">
            <p>Copyright &copy 2016<a href="http://www.github.com/mastopp" target="_blank"> Mastopp</a></p>
            <!-- <div class="aa-footer-payment">
              <span class="fa fa-cc-mastercard"></span>
              <span class="fa fa-cc-visa"></span>
              <span class="fa fa-paypal"></span>
              <span class="fa fa-cc-discover"></span>
            </div> -->
          </div>
        </div>
      </div>
      </div>
    </div>
  </footer>
  <!-- / footer -->
    <!--   <footer class="copyright">
        <div class="navbar navbar-inverse" role="navigation">
        	<div>
        	    <h5>Copyright &copy; 2016 Twelve Inc</h5>
        	</div>
        </div>
      </footer> -->

    <!-- jQuery -->
    <script src="bootstrap/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/js/jPages.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
